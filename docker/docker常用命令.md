
# docker 常用命令

## 操作镜像
1. 查看镜像列表：
docker images

2. 删除images，通过image的id来指定删除谁
docker rmi <镜像id>

## 操作容器

1. 通过服务来操作docker：重启、停止、状态、启动等
service docker restart/stop/status/start <容器id>

2. 停止所有容器
docker stop $(docker ps -a -q)

3. 查看容器列表：
docker ps -a

4. 查看容器配置信息
docker inspect <容器id简写>

5. 查看容器环境变量
docker exec mysql5.7 env
docker exec <容器id> env

6. 对容器进行操作：
docker start <容器id>
docker stop <容器id>

7. 删除一个容器（须先停止）：
docker rm <容器id>

8. 进入一个容器内部：
docker exec -it <容器ID> bash

9. 查看日志
docker logs -f <docker_id>


10. 启动一个容器：
简单启动：docker run -d -p <容器id:tag>

自定义配置的启动：

```sh
docker run -d \
    -p 8081:8080 \
    -v /home/tomcat/tomcat-8081/webapps:/usr/local/tomcat/webapps/ \
    -v /home/tomcat/tomcat-8081/logs:/usr/local/tomcat/logs/ \
    --network my-net \
    --ip 192.168.3.81 \
    --name tomcat_8081 \
    tomcat:8.5.35-jre8
```
* -p 8081:8080 设置了端口：8081是主机访问的端口，8080是容器内部端口
* -v 设置了目录映射：左边是主机目录，右边是容器内的映射，不知道容器内的目录结构，可以进入容器内查看
* --network my-net，设置所属网络，这是自己创建的网络名称
* --ip 设置ip地址
* --name 设置此容器的别名
* tomcat:8.5.35-jre8 ：镜像的名称和tag

# docker 容器内不能使用vi命令
apt-get update
apt-get install vim

# docker使用习惯
可以将tomcat安装到docker中，一般都是从官方docker镜像库拉下来一个新的images，之后在此基础上修改
例如tomcat，可以将tomcat的webapps、logs、conf目录等映射到主机上，如果8080端口也需要通过主机来访问，也可以对其映射。
但一般情况下，不需要对8080端口映射，因为可以通过虚拟网络，直接访问到容器内的8080端口，除非，想通过外网直接进行访问，一般我们使用nginx时，不需要映射8080端口

# 如果想修改容器内的配置文件
* 方法一：先不做任何映射目录，启动一个临时容器，之后复制出一份配置文件到一个约定的映射目录，修改配置后，再重新启动一个指定映射目录的容器即可。
命令：docker run -d -p 18080:8080 tomcat
docker cp 95852771facb:/usr/local/tomcat/conf /home/tomcat/tomcat-8081/conf

* 方法二：直接修改镜像文件并提交保持，也可在主机修改完复制到容器内（暂未尝试）


# dokcer磁盘管理
1. 查看磁盘使用情况
docker system df

2. 清理磁盘
docker system prune

3. 清理数据卷
https://www.cnblogs.com/sammyliu/p/5932996.html

docker volume ls
docker volume ls -qf dangling=true
docker volume rm $(docker volume ls -qf dangling=true)

# 通过Dockerfile构建docker镜像
1. 通过远程脚步构建
docker build https://github.com/docker-library/mongo/blob/6932ac255d29759af9a74c6931faeb02de0fe53e/4.0/windows/windowsservercore-1803/Dockerfile

2. 通过当前目录下Dockerfile文件构建
docker build -t mongo:4.0.4 .


# docker 保存和加载
docker save  mongo:3.6 | gzip > mongo-3.6.tar.gz
docker load -i mongo-3.6.tar.gz


docker save mongo:4-xenial | gzip > mongo-4-xenial.tar.gz
docker load -i mongo-4-xenial.tar.gz

https://hub.docker.com/r/jaspeen/oracle-11g/tags/
docker save jaspeen/oracle-11g | gzip > oracle-11g.tar.gz
docker load -i oracle-11g.tar.gz
