
# 前提条件：
1. 系统为win10专业版或企业版
2. 支持cpu虚拟化
3. 开启Hyper-v功能
win+x: 应用和功能->右边【系统和功能】->启用或关闭windows功能->勾选Hyper-v

# win10 安装docker
下载win版docker安装文件：https://www.docker.com/get-started
正常安装即可，（不建议注册账号登录，否则右键菜单显示超级慢）

## 配置：右键 setting
1. General ：除【Expose daemon ont tcp://localhost:2375 without TLS】 选项外，全部取消勾选
2. Shared Drives：勾选你想共享的目录，我选择的是D盘，正常会提示你输入系统密码，如果没有提示，点击Reset credentials
3. Advanced：调整cpu(4)，内存（4096mb），swap（1024mb）
    Disk image location:这一项我修改了默认的虚拟硬盘路径，没有使用默认的c盘
    Disk image max size :默认64g足够
4. Deamon：在Registry mirrors内增加国内镜像资源库 https://c1qf03af.mirror.aliyuncs.com
5. 剩余Network、Porxies、Kubernetes三页：默认即可

## 常见问题
1. docker start <容器id> 报错，尝试 右键docker小图标 restart试试。

# win10 安装 oracle
镜像介绍：https://dev.aliyun.com/detail.html?spm=5176.1972343.2.8.E6Cbr1&repoId=1969


## 重要提示：
1.镜像可以用，确定！
2.镜像比较大.6个多G，下载速度比较快，犹豫就千万别下。
3.oracle没有多余用户，没有多余设置

oracle 11.0.2 64bit 企业版 实例名: helowin
容器系统用户：root/helowin

## 拉取镜像进行安装
docker pull registry.cn-hangzhou.aliyuncs.com/helowin/oracle_11g

## 简单启动，复制相关文件 

```sh
docker run -d \
    --name oracle_11g \
    -p 1521:1521 \
    registry.cn-hangzhou.aliyuncs.com/helowin/oracle_11g

copy一份配置文件
docker cp 177:/home/oracle/app/oracle/oradata D:/workspace/docker/oracle11gr2/oradata
docker cp 177:/home/oracle/app/oracle/flash_recovery_area/helowin D:/workspace/docker/oracle11gr2/helowin
docker cp 177:/home/oracle/app/oracle/product/11.2.0/dbhome_2/network/admin D:/workspace/docker/oracle11gr2/admin
```


## 正式启动（进行容器内文件目录映射）
```sh
docker run -d \
    --name oracle_11g \
    -p 1521:1521 \
    -v D:/workspace/docker/oracle11gr2/oradata:/home/oracle/app/oracle/oradata \
    -v D:/workspace/docker/oracle11gr2/helowin:/home/oracle/app/oracle/flash_recovery_area/helowin \
    -v D:/workspace/docker/oracle11gr2/admin:/home/oracle/app/oracle/product/11.2.0/dbhome_2/network/admin \
    registry.cn-hangzhou.aliyuncs.com/helowin/oracle_11g
```

## 创建新用户：admin/admin

1. 进入容器：docker exec -it <容器ID> /bin/bash
2. 加载环境变量：source /home/oracle/.bash_profile
3. 登录：sqlplus /nolog
4. connect /as sysdba
5. 创建用户
```sql
create user admin identified by admin;
grant connect,resource,dba to admin;

--以下为我用到账号
create user bes identified by 12345;
grant connect,resource,dba to bes;

create user bjytsq identified by 12345;
grant connect,resource,dba to bjytsq;

create user htmp_sh identified by 12345;
grant connect,resource,dba to htmp_sh;

create user htmp_zj identified by 12345;
grant connect,resource,dba to htmp_zj;

create user rst2_base identified by 12345;
grant connect,resource,dba to rst2_base;

create user jyzx identified by 12345;
grant connect,resource,dba to jyzx;

create user shjydzpf identified by 12345;
grant connect,resource,dba to shjydzpf;

create user oebank identified by 12345;
grant connect,resource,dba to oebank;

```


## 修改字符集，镜像的字符集不是ZHS16GBK，我们服务器用但是ZHS16GBK，所以需要修改
```sh
docker exec -it <容器ID> /bin/bash
source /home/oracle/.bash_profile
sqlplus /nolog
conn /as sysdba;

select userenv('language') from dual;  --查看服务端字符集
SHUTDOWN IMMEDIATE  --关闭数据库
STARTUP MOUNT  --启动到 Mount
ALTER SYSTEM ENABLE RESTRICTED SESSION;
ALTER SYSTEM SET JOB_QUEUE_PROCESSES=0;
ALTER SYSTEM SET AQ_TM_PROCESSES=0;
ALTER DATABASE OPEN;
ALTER DATABASE CHARACTER SET INTERNAL_USE ZHS16GBK;
SHUTDOWN IMMEDIATE;
STARTUP;
select userenv('language') from dual;  --查看服务端字符集
```

## 创建dump目录（目的是为了适应dump方式导入和导出数据库，优点自行百度）
1. 进入容器：docker exec -it <容器id> bash
2. 挂载oracle环境变量：source /home/oracle/.bash_profile
3. 连接sqlplus
sqlplus /nolog
conn /as sysdba;

4. 创建dump目录
create or replace directory expdir as '/home/oracle/app/oracle/oradata/expdir';

第一个expdir为数据库虚拟目录名称
第二expdir是此虚拟目录映射到容器内的目录位置，此目录后面会手工创建

5. 退出sqlplus：exit
6. 在容器内创建对应的目录expdir
cd /home/oracle/app/oracle/oradata
mkdir expdir

## expdp方式导出数据库
```sql
expdp oebank/12345@helowin directory=expdir dumpfile=oebank.dump schemas=oebank
```

## impdp方式导入数据库
```sql
--如果要导出用户和导入用户不一致可加如下参数
remap_schema=oldUser:newUser 
--如果表空间不一致可以加如下参数，可以指定多个表空间
remap_tablespace=oldSpace1:newSpace1 remap_tablespace=oldSpace2:newSpace2
--查看表空间
select file_name,tablespace_name, bytes / 1024 / 1024 "bytes MB", maxbytes / 1024 / 1024 "maxbytes MB" from dba_data_files
```

```sh
impdp bes/12345@helowin directory=expdir dumpfile=bes.dump schemas=bes

impdp bjytsq/12345@helowin directory=expdir dumpfile=bjytsq.dump schemas=bjytsq remap_tablespace=BJYTSQ_TEST:USERS remap_tablespace=BJYTSQ:USERS

impdp htmp_sh/12345@helowin directory=expdir dumpfile=htmp_sh.dump schemas=htmp_sh

impdp htmp_zj/12345@helowin directory=expdir dumpfile=htmp_zj.dump schemas=htmp_zj

impdp rst2_base/12345@helowin directory=expdir dumpfile=rst2_base.dump schemas=rst2_base

impdp jyzx/12345@helowin directory=expdir dumpfile=jyzx.dump schemas=jyzx remap_tablespace=JYZX:USERS

impdp shjydzpf/12345@helowin directory=expdir dumpfile=SHJYDZPF.dump schemas=shjydzpf remap_tablespace=SHJYDZPF:USERS
```

## imp/exp 导入和导出数据库 
将文件放到D:\workspace\docker\oracle11gr2\oradata目录一下

cd /home/oracle/app/oracle/oradata

进入容器即可使用命令导入导出操作


## oracle常用命令
1. 查看lisner状态：lsnrctl status
2. 启动：lsnrctl start


# 安装docker管理工具：portainer
前提：开启docker setting->Genneral中的
【Expose daemon ont tcp://localhost:2375 without TLS】 选项
1. 拉取镜像
docker pull portainer/portainer

2. 运行
docker run -d \
    -p 9000:9000 \
    --restart=always \
    --name portainer \
    -v D:/workspace/docker/portainer/docker.sock:/var/run/docker.sock \
    -v D:/workspace/docker/portainer/data:/data \
    portainer/portainer

3. 浏览器访问：http://localhost:9000，设置初始密码
4. 连接方式选择 Remote
Name一栏随意输入，如：docker-prod01
Endpoint URL 一栏输入：docker.for.win.localhost:2375


# 修改oracle镜像
## 简单启动

docker run -d \
    --name oracle_11gR2_ZHS16GBK \
    -p 1522:1521 \
    registry.cn-hangzhou.aliyuncs.com/helowin/oracle_11g


## 修改字符集，镜像的字符集不是ZHS16GBK，我们服务器用但是ZHS16GBK，所以需要修改
```sh
docker exec -it 41 /bin/bash
source /home/oracle/.bash_profile
sqlplus /nolog
conn /as sysdba;

select userenv('language') from dual;  --查看服务端字符集

USERENV('LANGUAGE')
----------------------------------------------------
AMERICAN_AMERICA.AL32UTF8

SHUTDOWN IMMEDIATE  --关闭数据库
STARTUP MOUNT  --启动到 Mount
ALTER SYSTEM ENABLE RESTRICTED SESSION;
ALTER SYSTEM SET JOB_QUEUE_PROCESSES=0;
ALTER SYSTEM SET AQ_TM_PROCESSES=0;
ALTER DATABASE OPEN;
ALTER DATABASE CHARACTER SET INTERNAL_USE ZHS16GBK;
SHUTDOWN IMMEDIATE;
STARTUP;
select userenv('language') from dual;  --查看服务端字符集

USERENV('LANGUAGE')
----------------------------------------------------
AMERICAN_AMERICA.ZHS16GBK

```

## 修改实例名：helowin->orcl
https://blog.csdn.net/dreamer_good/article/details/80650442
https://www.cnblogs.com/wqswjx/p/5522551.html
