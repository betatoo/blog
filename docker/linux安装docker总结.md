# 参考文章
* http://blog.51cto.com/lookingdream/2062392
* https://www.cnblogs.com/niloay/p/6259650.html
* https://www.cnblogs.com/jmaly/p/7722863.html
* https://blog.csdn.net/a745233700/article/details/80452862

# docker镜像仓库
* 官网(太慢)：https://hub.docker.com
* 阿里：https://cr.console.aliyun.com

# 修改虚拟机ip（根据情况设置）
1. 进入文件目录
cd /etc/sysconfig/network-scripts
2. 编辑ifcfg-xxx文件
vi ifcfg-ens33
3. 退出
:wq
4. 重启网络服务
service network restart

# 安装docker
1. 如果已安装过，卸载旧版本
yum remove docker docker-common docker-selinux

2. 安装需要的依赖包
yum install -y yum-utils device-mapper-persistent-data

3. 配置稳定仓库
yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo

4. 安装
yum install docker-ce

5. 启动docker
systemctl start docker
或者
service docker start

6. 验证docker是否安装正确
docker run hello-world

7. 加速
修改或创建daemon.json文件

vim /etc/docker/daemon.json
加入：
```sh
{
    "registry-mirrors": [
        "https://c1qf03af.mirror.aliyuncs.com",
        "https://registry.docker-cn.com",
        "http://hub-mirror.c.163.com"
    ],
    "insecure-registries": []
}
```


# linux其他常用命令备忘

1. 删除无用的文件夹
进入容器内部
docker exec -it <容器id> bash
删除文件夹
rm -rf <文件夹名>
退出容器
exit

2. 端口占用情况
netstat –apn 

3. zip和unzip
安装zip unzip命令
yum install zip unzip

解压缩
unzip -o admin.zip -d /home/tomcat/tomcat-8081/webapps/ROOT/


# 创建桥接网络

```sh
docker network create \
    --driver bridge \
    --subnet 192.168.3.0/24 \
    --gateway=192.168.3.1 \
    my-net

```
可以在容器启动时，加入：--network=my-net --ip=192.168.3.8，来指定ip和所属网络

==注意：想要各个容器通过网络通信，必须放到一个网络内，可直接通过容器的ip和容器内部端口进行连接，不需要使用对外映射的端口==

* docker network的常用命令如下：
* docker network connect ： 将容器加入到指定网络中；
* docker network create <网络名字>： 创建网络；
* docker network disconnect ： 将容器中网络中移除；
* docker network inspect：查看指定网络的详情；
* docker network ls：列出所有网络；
* docker network rm：删除指定网络；


# 安装tomcat

1. 搜索镜像
docker search tomcat

2. 拉取指定版本的镜像
docker pull tomcat:8.5.35-jre8

3. 启动

创建目录
```sh
mkdir -p /home/tomcat/tomcat-8081/logs
mkdir -p /home/tomcat/tomcat-8081/webapps/ROOT

```

指定挂载目录，指定名称
```sh
docker run -d \
    -p 8081:8080 \
    -v /home/tomcat/tomcat-8081/webapps:/usr/local/tomcat/webapps/ \
    -v /home/tomcat/tomcat-8081/logs:/usr/local/tomcat/logs/ \
    --network my-net \
    --ip 192.168.3.81 \
    --name tomcat_8081 \
    tomcat:8.5.35-jre8

映射配置文件目录（可选）
    -v /home/tomcat/tomcat-8081/conf:/usr/local/tomcat/conf \
```

# 安装redis
```sh
docker pull redis:3.2
docker run -d \
    --ip=192.168.3.3 \
    --network my-net \
    --name redis3.2 \
    redis:3.2

```
此处没必要映射端口（-p 6379:6379），因为不需要通过外网访问redis内部，保证通过局域网访问redis即可满足需求

# 安装nginx
```sh
docker pull nginx:3.2

简单启动，目的是复制配置文件（需删除）
docker run -d --name nginx2 -d -p 81:80 nginx
docker cp 97:/etc/nginx/conf.d /home/nginx/conf.d/

重新启动一个新的
docker run -d \
    -p 80:80 \
    -v /home/nginx/conf.d/:/etc/nginx/conf.d \
    -v /home/nginx/home:/home \
    --network my-net \
    --ip 192.168.3.80 \
    --name nginx \
    nginx
```
进入容器内部，执行一些命令
docker exec -it nginx bash

校验和重新加载文件
nginx -t
nginx -s reload


# 安装mysql
1. 拉取镜像
docker pull mysql:5.7

2. 创建主机映射目录
mkdir -p /home/mysql/datadir #用于挂载mysql数据文件
mkdir -p /home/mysql/conf.d #用于挂载mysql配置文件(可不设置)

3. 运行
```sh
可简单运行，cp配置文件
docker run -d -p 3306:3306 -e MYSQL_ROOT_PASSWORD=root --name mysql5.7-test mysql:5.7

正式运行
docker run -d \
    -p 63336:3306 \
    -e MYSQL_ROOT_PASSWORD=root \
    -v /home/mysql/datadir:/var/lib/mysql \
    --network my-net \
    --ip 192.168.3.2 \
    --name mysql5.7 \
    mysql:5.7
```

4. mysql创建用户命令
进入容器内部
docker exec -it <容器id> bash

执行mysql命令
mysql --host=<容器内系统名称，即容器id> -u root -p

查看用户使用情况
user mysql;
select host, user from user;

创建用户
CREATE USER 'test'@'%' IDENTIFIED BY 'test12345';

授权
grant all privileges on test.* to test@localhost identified by 'test12345';

刷新缓存
flush privileges;

# linux主机内配置java环境变量

unzip -o jre-8u191-linux-x64.zip -d /home/java/
编辑/etc/profile文件

export JRE_HOME=/home/java/jre1.8.0_191   
export CLASSPATH=.:$JRE_HOME/lib:$CLASSPATH   
export PATH=$JRE_HOME/bin:$PATH

执行命令，使环境变量生效
source /etc/profile


以下为网上资料
```sh
#set java environment   
export JAVA_HOME=/usr/my/jdk/jdk1.8.0_144  
export JRE_HOME=$JAVA_HOME/jre   
export CLASSPATH=.:$JAVA_HOME/lib:$JRE_HOME/lib:$CLASSPATH   
export PATH=$JAVA_HOME/bin:$JRE_HOME/bin:$JAVA_HOME:$PATH

export JAVA_HOME=/usr/java/jdk1.8.0_101
export PATH=$JAVA_HOME/bin:$PATH
export CLASSPATH=.:$JAVA_HOME/lib/dt.jar:$JAVA_HOME/lib/tools.jar
```

环境变量配置内容添加位置有两种路径文件
1. /etc/profile
2 ./root/.bashrc       //bashrc文件是root文件夹下的隐藏文件

配置完后执行命令立即生效:
source /etc/profile
source /root/.bashrc

Linux下两种配置位置都是有效的 重启也是有效
但是在docker cento7环境下采用第一种配置重启后就失效了,应该是docker环境下,启动centos并不会执行/etc/profile文件
这里采用第二种配置重启jdk环境也可生效.

# linux主机运行jar文件
```sh
nohup java -jar /home/tomcat/test1.jar >/home/tomcat/log-test1.out &
运行完，回车即可

```
两种方式查看运行状态：
方法一：
查看后台执行的作业：jobs
调回到前台：fg <编号>

方法二：
查看进程：ps -aux | grep java
杀死进程：kill -s 9 <pid>


# 安装docker管理工具：portainer

docker pull portainer/portainer

docker run -d \
    -p 9000:9000 \
    --network my-net \
    --ip 192.168.3.90 \
    --restart=always \
    --name portainer \
    -v /var/run/docker.sock:/var/run/docker.sock \
    -v /home/portainer/data:/data \
    portainer/portainer


# MongoDB(未测试)

--这里的--name 放在前面并映射端口
--auth表示连接mongodb需要授权
docker run -d \
    --name mongodb \
    -p 27017:27017 \
    -v /home/mongodb/data/db/:/data/db \
    mongo:3.6 \
    --auth


//进入容器
docker exec -it  /bin/bash     
 
mongo
use admin
//创建用户,此用户创建成功,则后续操作都需要用户认证
db.createUser({user:"root",pwd:"root",roles:[{role:'root',db:'admin'}]})

db.createUser({user:"jinshuju",pwd:"jinshuju",roles:[{role:'readWrite',db:'jinshuju'}]})
exit 

测试:
mongo 宿主机ip/admin -utest -p
