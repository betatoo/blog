
进入mongodb的bin目录下

# 无权限验证模式启动启动

mongod --port=27017 --dbpath=../db --directoryperdb

# 创建用户
mongo
use admin

//创建用户,此用户创建成功,则后续操作都需要用户认证
db.createUser({user:"root",pwd:"root",roles:[{role:'root',db:'admin'}]})
db.auth('root','root')

//创建jinshuju数据库，切换到jinshuju
use jinshuju

//创建用户，并赋读写权限
db.createUser({user:"jinshuju",pwd:"jinshuju",roles:[{role:'readWrite',db:'jinshuju'}]})


2. 权限验证模式启动
mongod --port=27017 --dbpath=../db --auth --directoryperdb

mongo
use admin
db.auth('root','root')

# Built-In Roles（内置角色）：
1. 数据库用户角色：read、readWrite;
2. 数据库管理角色：dbAdmin、dbOwner、userAdmin；
3. 集群管理角色：clusterAdmin、clusterManager、clusterMonitor、hostManager；
4. 备份恢复角色：backup、restore；
5. 所有数据库角色：readAnyDatabase、readWriteAnyDatabase、userAdminAnyDatabase、dbAdminAnyDatabase
6. 超级用户角色：root  
// 这里还有几个角色间接或直接提供了系统超级用户的访问（dbOwner 、userAdmin、userAdminAnyDatabase）
7. 内部角色: ```__system```



# 角色权限
* Read：允许用户读取指定数据库
* readWrite：允许用户读写指定数据库
* dbAdmin：允许用户在指定数据库中执行管理函数，如索引创建、删除，查看统计或访问system.profile
* userAdmin：允许用户向system.users集合写入，可以找指定数据库里创建、删除和管理用户
* clusterAdmin：只在admin数据库中可用，赋予用户所有分片和复制集相关函数的管理权限。
* readAnyDatabase：只在admin数据库中可用，赋予用户所有数据库的读权限
* readWriteAnyDatabase：只在admin数据库中可用，赋予用户所有数据库的读写权限
* userAdminAnyDatabase：只在admin数据库中可用，赋予用户所有数据库的userAdmin权限
* dbAdminAnyDatabase：只在admin数据库中可用，赋予用户所有数据库的dbAdmin权限。
* root：只在admin数据库中可用。超级账号，超级权限


MongoDB启动命令mongod参数说明：

# 基本配置

--quiet	# 安静输出
--port arg	# 指定服务端口号，默认端口27017
--bind_ip arg	# 绑定服务IP，若绑定127.0.0.1，则只能本机访问，不指定默认本地所有IP
--logpath arg	# 指定MongoDB日志文件，注意是指定文件不是目录
--logappend	# 使用追加的方式写日志
--pidfilepath arg	# PID File 的完整路径，如果没有设置，则没有PID文件
--keyFile arg	# 集群的私钥的完整路径，只对于Replica Set 架构有效
--unixSocketPrefix arg	# UNIX域套接字替代目录,(默认为 /tmp)
--fork	# 以守护进程的方式运行MongoDB，创建服务器进程
--auth	# 启用验证
--cpu	# 定期显示CPU的CPU利用率和iowait
--dbpath arg	# 指定数据库路径
--diaglog arg	# diaglog选项 0=off 1=W 2=R 3=both 7=W+some reads
--directoryperdb	# 设置每个数据库将被保存在一个单独的目录
--journal	# 启用日志选项，MongoDB的数据操作将会写入到journal文件夹的文件里
--journalOptions arg	# 启用日志诊断选项
--ipv6	# 启用IPv6选项
--jsonp	# 允许JSONP形式通过HTTP访问（有安全影响）
--maxConns arg	# 最大同时连接数 默认2000
--noauth	# 不启用验证
--nohttpinterface	# 关闭http接口，默认关闭27018端口访问
--noprealloc	# 禁用数据文件预分配(往往影响性能)
--noscripting	# 禁用脚本引擎
--notablescan	# 不允许表扫描
--nounixsocket	# 禁用Unix套接字监听
--nssize arg (=16)	# 设置信数据库.ns文件大小(MB)
--objcheck	# 在收到客户数据,检查的有效性，
--profile arg	# 档案参数 0=off 1=slow, 2=all
--quota	# 限制每个数据库的文件数，设置默认为8
--quotaFiles arg	# number of files allower per db, requires --quota
--rest	# 开启简单的rest API
--repair	# 修复所有数据库run repair on all dbs
--repairpath arg	# 修复库生成的文件的目录,默认为目录名称dbpath
--slowms arg (=100)	# value of slow for profile and console log
--smallfiles	# 使用较小的默认文件
--syncdelay arg (=60)	# 数据写入磁盘的时间秒数(0=never,不推荐)
--sysinfo	# 打印一些诊断系统信息
--upgrade	# 如果需要升级数据库