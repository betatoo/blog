# 安装nvm

1. 设置环境变量
NVM_HOME=d:\nodejs\nvm-noinstall
NVM_SYMLINK=d:\nodejs\nodejs

2. settings.txt文件内容如下
```sh
root:  d:\nodejs\nvm-noinstall
path:  d:\nodejs\nodejs
arch: 64 
proxy: none
```

3. nodejs官网下载解压版nodejs，解压到d:\nodejs\nvm-noinstall目录下
执行命令：nvm list，可看到nodejs版本列表，如：v10.13.0
执行：nvm use v10.13.0，即可启用此版本nodejs

4. nvm 常用命令
查看系统nodejs版本环境：nvm list
启用某版本nodejs：nvm use 版本号

# nodejs环境设置

1. 设置npm源为淘宝镜像，加快npm包安装速度
npm config set registry https://registry.npm.taobao.org
查看配置结果：npm config get registry


# 新建一个react简单项目
npm i --save-dev react
